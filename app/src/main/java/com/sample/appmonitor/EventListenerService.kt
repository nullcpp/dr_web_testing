package com.sample.appmonitor

import android.app.*
import android.content.BroadcastReceiver
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.os.IBinder
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.sample.appmonitor.database.EventType
import com.sample.appmonitor.database.EventsDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext


class EventListenerService : Service() , CoroutineScope {
    private val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    private var eventReceiver: BroadcastReceiver? = null
    private val database: EventsDatabase = App.getDatabase()

    override fun onCreate() {
        super.onCreate()

        createNotificationChannel()
        showStartupNotification()

        eventReceiver = AppEventBroadcastReceiver(this) {
            launch {
                val eid = database.eventsDao().insert(it)
                val eventText = when (it.type) {
                    EventType.ADD -> "install"
                    EventType.REPLACE -> "update"
                }
                val intent = EventDetailActivity.getStartIntent(this@EventListenerService, it)
                val pendingIntent = PendingIntent.getActivity(this@EventListenerService, eid.toInt(), intent, 0)
                val notification = notificationBuilder("New $eventText event!", "App: ${it.appName}", pendingIntent)
                showNotification(notification.setAutoCancel(true).build(), eid.toInt())
            }
        }

        val filter = IntentFilter()
        filter.addAction(Intent.ACTION_PACKAGE_ADDED)
        filter.addDataScheme("package")
        registerReceiver(eventReceiver, filter)
    }

    private fun showStartupNotification() {
        val fgNotification = notificationBuilder(getString(R.string.app_name), "Waiting new events")
        val stopSelf = Intent(this, EventListenerService::class.java)
        stopSelf.action = ACTION_STOP_SERVICE
        val pStopSelf = PendingIntent.getService(this, 0, stopSelf, PendingIntent.FLAG_CANCEL_CURRENT)
        fgNotification.addAction(R.drawable.ic_block_black_24dp, "Stop", pStopSelf)
        startForeground(1, fgNotification.build())
    }

    override fun onDestroy() {
        unregisterReceiver(eventReceiver)
        job.cancel()
        super.onDestroy()
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (ACTION_STOP_SERVICE == intent?.action) {
            stopForeground(true)
            stopSelf()
            return START_NOT_STICKY
        }
        return START_STICKY // Don't kill me!
    }

    private fun showNotification(notification: Notification, id: Int) {
        val manager = NotificationManagerCompat.from(this)
        val nid = Math.min(Int.MAX_VALUE, NOTIFICATION_ID + id)
        Log.d("TAG", "not id: $nid")
        manager.notify(nid, notification)
    }

    private fun notificationBuilder(title: String, text: String) = notificationBuilder(title, text, null)

    private fun notificationBuilder(title: String, text: String, intent: PendingIntent?): NotificationCompat.Builder {
        return NotificationCompat.Builder(applicationContext, CHANNEL_ID)
            .setContentTitle(title)
            .setContentText(text)
            .setSmallIcon(R.drawable.ic_event_note_black_24dp)
            .setContentIntent(intent)
            .setOnlyAlertOnce(false)
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.channel_name)
            val descriptionText = getString(R.string.channel_description)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(CHANNEL_ID, name, importance).apply {
                description = descriptionText
            }
            val notificationManager = NotificationManagerCompat.from(this)
            notificationManager.createNotificationChannel(channel)
        }
    }

    companion object {
        private const val CHANNEL_ID = "app_monitor_channel"
        private const val NOTIFICATION_ID = 1337
        private const val ACTION_STOP_SERVICE = "action_stop_service"
    }
}
