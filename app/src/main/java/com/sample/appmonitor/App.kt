package com.sample.appmonitor

import android.app.Application
import com.sample.appmonitor.database.EventsDatabase

class App: Application() {

    override fun onCreate() {
        super.onCreate()

        eventsDatabase = EventsDatabase.build(this)
    }

    companion object {
        private lateinit var eventsDatabase: EventsDatabase
        public fun getDatabase(): EventsDatabase {
            return  eventsDatabase
        }
    }
}