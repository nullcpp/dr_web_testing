package com.sample.appmonitor.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.sample.appmonitor.R
import com.sample.appmonitor.database.EventType
import com.sample.appmonitor.database.entity.EventEntity
import com.sample.appmonitor.database.entity.appLauncherIcon
import com.sample.appmonitor.databinding.ItemEventEntityBinding
import java.text.SimpleDateFormat
import java.util.*

class EventViewHolder(val ui: ItemEventEntityBinding, private val clickListener: (Int) -> Unit): RecyclerView.ViewHolder(ui.root) {

    init {
        ui.root.setOnClickListener {
            clickListener(adapterPosition)
        }
    }

    fun bindTo(item: EventEntity) {
        ui.icon.setImageDrawable(item.appLauncherIcon(ui.root.context))
        val eventType = when(item.type) {
            EventType.ADD -> "Installed"
            EventType.REPLACE -> "Updated"
        }
        val sdf = SimpleDateFormat("HH:mm:ss dd.MM.yyyy", Locale.getDefault())
        val dateText = sdf.format(item.date)
        ui.title.text = "${item.appName}\n$eventType at $dateText"
    }

    companion object {
        fun create(parent: ViewGroup, itemClickListener: (Int) -> Unit): EventViewHolder {
            val ui:ItemEventEntityBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context),
                R.layout.item_event_entity, parent, false)
            return EventViewHolder(ui, itemClickListener)
        }
    }
}