package com.sample.appmonitor

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.sample.appmonitor.database.EventType
import com.sample.appmonitor.database.entity.EventEntity
import com.sample.appmonitor.database.entity.appLauncherIcon
import com.sample.appmonitor.database.entity.launchApp
import com.sample.appmonitor.databinding.ActivityEventDetailBinding
import java.util.*

class EventDetailActivity : AppCompatActivity() {
    private lateinit var ui: ActivityEventDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ui = DataBindingUtil.setContentView(this, R.layout.activity_event_detail)

        val item: EventEntity? = intent?.extras?.getParcelable(EVENT)

        if (item == null) {
            finish()
            return
        }

        ui.icon.setImageDrawable(item.appLauncherIcon(this))
        ui.appName.text = "App name: ${item.appName}"
        ui.version.text = "Version: ${item.version}"
        ui.eventType.text = "Event type: " + when (item.type) {
            EventType.ADD -> "Install"
            EventType.REPLACE -> "Update"
        }
        ui.eventDate.text = Date(item.date).toString()
        ui.sha1hash.text = "Apk SHA1: ${item.sha1}"
        ui.appLaunch.setOnClickListener {
            item.launchApp(this)
        }
    }

    companion object {
        fun getStartIntent(context: Context, data: EventEntity): Intent {
            val intent = Intent(context, EventDetailActivity::class.java)
            intent.putExtra(EVENT, data)
            return intent
        }

        const val EVENT = "event"
    }
}
