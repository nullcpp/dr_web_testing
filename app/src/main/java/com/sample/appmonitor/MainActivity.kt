package com.sample.appmonitor

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sample.appmonitor.databinding.ActivityMainBinding
import com.sample.appmonitor.ui.EventsAdapter

class MainActivity : AppCompatActivity() {
    private lateinit var ui: ActivityMainBinding
    private val db = App.getDatabase()

    private val adapter = EventsAdapter {
        Log.d("TAG", "Clicked $it")
        val intent = Intent(this, EventDetailActivity::class.java)
        intent.putExtra(EventDetailActivity.EVENT, it)
        startActivity(intent)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ui = DataBindingUtil.setContentView(this, R.layout.activity_main)

        val intent = Intent(this, EventListenerService::class.java)
        startService(intent)

        val layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        ui.list.addItemDecoration(DividerItemDecoration(this, LinearLayout.VERTICAL))
        ui.list.layoutManager = layoutManager
        ui.list.adapter = adapter
        adapter.registerAdapterDataObserver( object: RecyclerView.AdapterDataObserver() {

            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                super.onItemRangeInserted(positionStart, itemCount)
                ui.list.scrollToPosition(0)
            }
        })

        db.eventsDao().getlAllLive().observe(this, Observer {
            adapter.submitList(it)
        })

    }
}
