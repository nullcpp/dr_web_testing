package com.sample.appmonitor

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.util.Log
import androidx.core.graphics.drawable.toBitmap
import com.sample.appmonitor.database.EventType
import com.sample.appmonitor.database.entity.EventEntity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import java.io.*
import java.math.BigInteger
import java.nio.ByteBuffer
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.*
import kotlin.coroutines.CoroutineContext


class AppEventBroadcastReceiver(
    private var context: Context,
    private var handler: (EventEntity) -> Unit
) : BroadcastReceiver(), CoroutineScope {

    override val coroutineContext: CoroutineContext = Dispatchers.IO

    private val TAG = javaClass.simpleName
    private val eventMapper: Map<String, (Intent) -> Unit> = mapOf(
        Intent.ACTION_PACKAGE_ADDED to {intent -> onPackageAdded(intent)}
    )

    override fun onReceive(context: Context, intent: Intent) {
        this.context = context.applicationContext
        val action = intent.action
        if (action.isNullOrEmpty()) {
            return
        }
        eventMapper[action]?.invoke(intent)
    }

    private fun onPackageAdded(intent: Intent) {
        val data = intent.data
        val isReplacing = intent.getBooleanExtra(Intent.EXTRA_REPLACING, false)
        val eventType = if (!isReplacing) {
            EventType.ADD
        } else {
            EventType.REPLACE
        }
        val entity = fetchPackageInfo(eventType, context, data?.encodedSchemeSpecificPart )
        handler(entity)
        Log.d(TAG, "Package handled: $entity")
        val intent = context.packageManager.getLaunchIntentForPackage(entity.packageName)
        context.startActivity(intent)
    }

    private fun fetchPackageInfo(mode: EventType, context: Context, packageName: String?): EventEntity {
        val pm = context.packageManager
        val packageInfo = pm.getPackageInfo(packageName, 0)
        val apkPath = packageInfo.applicationInfo.sourceDir

        val sha1Hash = computeSha1(apkPath)
        val pkgName = packageInfo.applicationInfo.packageName
        val iconPath = saveAppIcon(pm.getApplicationIcon(pkgName), sha1Hash)
        val appName = pm.getApplicationLabel(packageInfo.applicationInfo).toString()
        val version = packageInfo.versionName
        val date = Date().time
        Log.d(TAG, "Apk path: $apkPath")
        Log.d(TAG, "Application name: $appName")
        Log.d(TAG, "package name: $pkgName")
        Log.d(TAG, "Event date: ${Date()}")
        Log.d(TAG, "Version: $version")
        Log.d(TAG, "Apk SHA1: $sha1Hash")

        val entity = EventEntity(mode, date, pkgName, iconPath, appName, version, sha1Hash)
        return entity
    }

    private fun saveAppIcon(icon: Drawable?, name: String): String {
        icon ?: return ""
        val bitmap = icon.toBitmap()
        val outputFile = File(context!!.filesDir, "$name.png")
        var outputPath = outputFile.toString()
        try {
            FileOutputStream(outputFile).use {
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, it)
            }
        } catch (e: FileNotFoundException) {
            outputPath = ""
        }

        return outputPath
    }

    private fun computeSha1(path: String): String {
        val digest: MessageDigest
        var output = ""
        try {
            digest = MessageDigest.getInstance("SHA-1")
        } catch (e: NoSuchAlgorithmException) {
            Log.e(TAG, "Exception while getting Digest", e)
            return output
        }

        try {
            FileInputStream(path).use {
                val channel = it.channel
                val byteBuffer = ByteBuffer.allocateDirect(8192)
                val bytes = ByteArray(8192)

                var read = channel.read(byteBuffer)
                while (read > 0) {
                    byteBuffer.flip()
                    byteBuffer.get(bytes, 0, read)
                    digest.update(bytes, 0, read)
                    byteBuffer.clear()
                    read = channel.read(byteBuffer)
                }
                val md5sum = digest.digest()
                val bigInt = BigInteger(1, md5sum)
                output = bigInt.toString(16)
                output = String.format("%40s", output).replace(' ', '0')

                Log.i(TAG, "Generated: $output")
            }
        } catch (e: IOException) {
            Log.e(TAG, "Exception while getting FileInputStream", e)
        }

        return output
    }
}