package com.sample.appmonitor.database.entity

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Parcel
import android.os.Parcelable
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.sample.appmonitor.database.EventType

@Entity
data class EventEntity(
    val type: EventType,
    val date: Long,
    val packageName: String,
    val iconPath: String,
    val appName: String,
    val version: String,
    val sha1: String
): Parcelable {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

    constructor(parcel: Parcel) : this(
        EventType.valueOf(parcel.readString()!!),
        parcel.readLong(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
        id = parcel.readInt()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(type.name)
        parcel.writeLong(date)
        parcel.writeString(packageName)
        parcel.writeString(iconPath)
        parcel.writeString(appName)
        parcel.writeString(version)
        parcel.writeString(sha1)
        parcel.writeInt(id)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<EventEntity> {
        override fun createFromParcel(parcel: Parcel): EventEntity {
            return EventEntity(parcel)
        }

        override fun newArray(size: Int): Array<EventEntity?> {
            return arrayOfNulls(size)
        }
    }
}

infix fun EventEntity.launchApp(context: Context) {
    val intent = context.packageManager.getLaunchIntentForPackage(this.packageName)
    if (intent == null) {
        Toast.makeText(context, "App start intent not found!", Toast.LENGTH_SHORT).show()
        return
    }
    ContextCompat.startActivity(context, intent, null)
}

infix fun EventEntity.appLauncherIcon(context: Context): Drawable {
    var drawable = Drawable.createFromPath(iconPath)
    return drawable
}