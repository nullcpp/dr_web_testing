package com.sample.appmonitor.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.sample.appmonitor.database.dao.EventsEntityDao
import com.sample.appmonitor.database.entity.Converters
import com.sample.appmonitor.database.entity.EventEntity

@Database(version = 1, entities = [EventEntity::class])
@TypeConverters(Converters::class)
abstract class EventsDatabase : RoomDatabase() {

    abstract fun eventsDao(): EventsEntityDao

    companion object {
        fun build(context: Context) : EventsDatabase {
            val db = Room.databaseBuilder(context, EventsDatabase::class.java, "app_events").build()
            return db
        }
    }
}