package com.sample.appmonitor.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.sample.appmonitor.database.entity.EventEntity

@Dao
interface EventsEntityDao {

    @Query("SELECT * FROM EventEntity ORDER BY date DESC")
    suspend fun getlAll(): List<EventEntity>

    @Query("SELECT * FROM EventEntity ORDER BY date DESC")
    fun getlAllLive(): LiveData<List<EventEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entity: EventEntity): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entities: List<EventEntity>)
}