package com.sample.appmonitor.database

enum class EventType {
    ADD,
    REPLACE
}