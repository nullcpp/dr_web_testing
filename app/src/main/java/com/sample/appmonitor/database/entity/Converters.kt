package com.sample.appmonitor.database.entity

import androidx.room.TypeConverter
import com.sample.appmonitor.database.EventType


class Converters {

    @TypeConverter
    fun fromEventType(value: EventType): String {
        return value.name
    }

    @TypeConverter
    fun toEventType(value: String): EventType {
        return EventType.valueOf(value)
    }
}